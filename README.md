# Welcome to the repository of PyNet

![banner](https://i.ibb.co/9TBFRqH/pynet-banner.png)

# What is the purpose of this repository?

PyNet is a collaboration project designed to help us (the contributors) grow our knowledge of data science by creating a Python neural network from scratch. 

# Open-Source Guidelines

Please review our [Code of Conduct](CONTRIBUTING.md)

# License

[GPL-3.0](LICENSE.md)

